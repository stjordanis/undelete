# Undelete

Undelete allows to undelete erased (but recoverable) files from a FAT16/FAT32 filesystem.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## UNDELETE.LSM

<table>
<tr><td>title</td><td>Undelete</td></tr>
<tr><td>version</td><td>2008a</td></tr>
<tr><td>entered&nbsp;date</td><td>2009-04-05</td></tr>
<tr><td>description</td><td>Undelete allows to undelete erased (but recoverable) files from a FAT16/FAT32 filesystem.</td></tr>
<tr><td>keywords</td><td>undelete, unerase, recovery</td></tr>
<tr><td>author</td><td>Eric Auer</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Undelete</td></tr>
</table>
